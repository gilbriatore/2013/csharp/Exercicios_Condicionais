﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_05
{
    public partial class frmExercicio06 : Form
    {
        public frmExercicio06()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int dep;
            float bruto, inss, fam, liquido;
            if (txtFuncionario.Text != "" && txtDependentes.Text != "" && txtBruto.Text != "")
            {
                dep = int.Parse(txtDependentes.Text);
                bruto = float.Parse(txtBruto.Text);
                if (bruto <= 800)
                {
                    inss = bruto * 7 / 100;
                }
                else
                {
                    if (bruto <= 1200)
                    {
                        inss = bruto * 8 / 100;
                    }
                    else
                    {
                        if (bruto <= 1500)
                        {
                            inss = bruto * 9 / 100;
                        }
                        else
                        {
                            inss = 135;
                        }
                    }
                }
                fam = 0;
                if (bruto <= 450)
                {
                    fam = dep * 25;
                }
                else
                {
                    if (bruto <= 600)
                    {
                        fam = dep * 20;
                    }
                }
                txtInss.Text = inss.ToString("C2");
                txtFamilia.Text = fam.ToString("C2");
                liquido = bruto - inss - fam;
                txtLiquido.Text = liquido.ToString("C2");
            }
            else{
                MessageBox.Show("Todos os campos são de preenchimento obrigatório", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtInss.Clear();
                txtFamilia.Clear();
                txtLiquido.Clear();
                txtFuncionario.Focus();
            }
        }

        private void frmExercicio06_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Encerrar o programa?", "Finalizar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}
