﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_05
{
    public partial class frmExercicio05 : Form
    {
        public frmExercicio05()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int v1, v2, res;
            if (txtPrimeiro.Text != "" && txtSegundo.Text != "")
            {
                v1 = int.Parse(txtPrimeiro.Text);
                v2 = int.Parse(txtSegundo.Text);
                if (v1 > v2)
                {
                    res = v1 - v2;
                }
                else
                {
                    res = v2 - v1;
                }
                txtResultado.Text = res.ToString();
            }
            else{
                MessageBox.Show("Todos os campos são de preenchimento obrigatório", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtResultado.Clear();
                txtPrimeiro.Focus();
            }
        }

        private void frmExercicio05_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Encerrar o programa?", "Finalizar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No){
                e.Cancel = true;
            }
        }
    }
}
