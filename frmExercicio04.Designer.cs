﻿namespace Aula_05
{
    partial class frmExercicio04
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalcular = new System.Windows.Forms.Button();
            this.txtLiquido = new System.Windows.Forms.TextBox();
            this.lblLiquido = new System.Windows.Forms.Label();
            this.txtBruto = new System.Windows.Forms.TextBox();
            this.lblBruto = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(108, 72);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 21;
            this.btnCalcular.Text = "&Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // txtLiquido
            // 
            this.txtLiquido.Enabled = false;
            this.txtLiquido.Location = new System.Drawing.Point(100, 40);
            this.txtLiquido.Name = "txtLiquido";
            this.txtLiquido.Size = new System.Drawing.Size(84, 20);
            this.txtLiquido.TabIndex = 20;
            this.txtLiquido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblLiquido
            // 
            this.lblLiquido.Location = new System.Drawing.Point(16, 40);
            this.lblLiquido.Name = "lblLiquido";
            this.lblLiquido.Size = new System.Drawing.Size(76, 20);
            this.lblLiquido.TabIndex = 19;
            this.lblLiquido.Text = "Salário liquido";
            // 
            // txtBruto
            // 
            this.txtBruto.Location = new System.Drawing.Point(100, 16);
            this.txtBruto.Name = "txtBruto";
            this.txtBruto.Size = new System.Drawing.Size(84, 20);
            this.txtBruto.TabIndex = 18;
            this.txtBruto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblBruto
            // 
            this.lblBruto.Location = new System.Drawing.Point(16, 16);
            this.lblBruto.Name = "lblBruto";
            this.lblBruto.Size = new System.Drawing.Size(76, 20);
            this.lblBruto.TabIndex = 17;
            this.lblBruto.Text = "Salário bruto";
            // 
            // frmExercicio04
            // 
            this.AcceptButton = this.btnCalcular;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(206, 118);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.txtLiquido);
            this.Controls.Add(this.lblLiquido);
            this.Controls.Add(this.txtBruto);
            this.Controls.Add(this.lblBruto);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExercicio04";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exercício 04";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmExercicio04_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.TextBox txtLiquido;
        private System.Windows.Forms.Label lblLiquido;
        private System.Windows.Forms.TextBox txtBruto;
        private System.Windows.Forms.Label lblBruto;
    }
}