﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_05
{
    public partial class frmExercicio09 : Form
    {
        public frmExercicio09()
        {
            InitializeComponent();
        }

        private void btnVerificar_Click(object sender, EventArgs e)
        {
            int valor;
            string mensagem = "";
            if (txtValor.Text != ""){
                valor = int.Parse(txtValor.Text);
                if(valor % 2 == 0){
                    mensagem = "Par";
                }
                else{
                    mensagem = "Ímpar";
                }
                if(valor > 0){
                    mensagem = mensagem + " / Positivo";
                }
                else{
                    mensagem = mensagem + " / Negativo";
                }
                txtResultado.Text = mensagem;
            }
            else{
                MessageBox.Show("Todos os campos são de preenchimento obrigatório","Alerta",MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtResultado.Clear();
                txtValor.Focus();
            }
        }

        private void frmExercicio09_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Encerrar o programa?", "Finalizar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}
