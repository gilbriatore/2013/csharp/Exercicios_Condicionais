﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_05
{
    public partial class frmExercicio01 : Form
    {
        public frmExercicio01()
        {
            InitializeComponent();
        }

        private void btnVerificar_Click(object sender, EventArgs e)
        {
            int idade;
            if (txtNome.Text != "" && txtIdade.Text != "")
            {
                idade = int.Parse(txtIdade.Text);
                if (idade >= 18)
                {
                    txtResultado.Text = "Pode tirar carteira";
                }
                else
                {
                    txtResultado.Text = "Não pode tirar carteira";
                }
            }
            else{
                MessageBox.Show("Todos os campos são de preenchimento obrigatório", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtResultado.Clear();
                txtNome.Focus();
            }
        }

        private void frmExercicio01_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Finalizar o programa?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No){
                e.Cancel = true;
            }
        }
    }
}
