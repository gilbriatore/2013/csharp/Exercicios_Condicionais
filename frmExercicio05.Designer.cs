﻿namespace Aula_05
{
    partial class frmExercicio05
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPrimeiro = new System.Windows.Forms.TextBox();
            this.lblPrimeiro = new System.Windows.Forms.Label();
            this.txtSegundo = new System.Windows.Forms.TextBox();
            this.lblSegundo = new System.Windows.Forms.Label();
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.lblResultado = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtPrimeiro
            // 
            this.txtPrimeiro.Location = new System.Drawing.Point(96, 20);
            this.txtPrimeiro.Name = "txtPrimeiro";
            this.txtPrimeiro.Size = new System.Drawing.Size(84, 20);
            this.txtPrimeiro.TabIndex = 20;
            this.txtPrimeiro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblPrimeiro
            // 
            this.lblPrimeiro.Location = new System.Drawing.Point(12, 20);
            this.lblPrimeiro.Name = "lblPrimeiro";
            this.lblPrimeiro.Size = new System.Drawing.Size(76, 20);
            this.lblPrimeiro.TabIndex = 19;
            this.lblPrimeiro.Text = "Primeiro valor";
            // 
            // txtSegundo
            // 
            this.txtSegundo.Location = new System.Drawing.Point(96, 48);
            this.txtSegundo.Name = "txtSegundo";
            this.txtSegundo.Size = new System.Drawing.Size(84, 20);
            this.txtSegundo.TabIndex = 22;
            this.txtSegundo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblSegundo
            // 
            this.lblSegundo.Location = new System.Drawing.Point(12, 48);
            this.lblSegundo.Name = "lblSegundo";
            this.lblSegundo.Size = new System.Drawing.Size(76, 20);
            this.lblSegundo.TabIndex = 21;
            this.lblSegundo.Text = "Segundo valor";
            // 
            // txtResultado
            // 
            this.txtResultado.Enabled = false;
            this.txtResultado.Location = new System.Drawing.Point(96, 76);
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.Size = new System.Drawing.Size(84, 20);
            this.txtResultado.TabIndex = 24;
            this.txtResultado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblResultado
            // 
            this.lblResultado.Location = new System.Drawing.Point(12, 76);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(76, 20);
            this.lblResultado.TabIndex = 23;
            this.lblResultado.Text = "Resultado";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(104, 104);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 25;
            this.btnCalcular.Text = "&Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // frmExercicio05
            // 
            this.AcceptButton = this.btnCalcular;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(190, 143);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.txtResultado);
            this.Controls.Add(this.lblResultado);
            this.Controls.Add(this.txtSegundo);
            this.Controls.Add(this.lblSegundo);
            this.Controls.Add(this.txtPrimeiro);
            this.Controls.Add(this.lblPrimeiro);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExercicio05";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exercicio 05";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmExercicio05_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPrimeiro;
        private System.Windows.Forms.Label lblPrimeiro;
        private System.Windows.Forms.TextBox txtSegundo;
        private System.Windows.Forms.Label lblSegundo;
        private System.Windows.Forms.TextBox txtResultado;
        private System.Windows.Forms.Label lblResultado;
        private System.Windows.Forms.Button btnCalcular;
    }
}