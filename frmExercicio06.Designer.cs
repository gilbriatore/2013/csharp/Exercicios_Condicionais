﻿namespace Aula_05
{
    partial class frmExercicio06
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDependentes = new System.Windows.Forms.TextBox();
            this.lblDependentes = new System.Windows.Forms.Label();
            this.txtFuncionario = new System.Windows.Forms.TextBox();
            this.lblFuncionario = new System.Windows.Forms.Label();
            this.txtBruto = new System.Windows.Forms.TextBox();
            this.lblBruto = new System.Windows.Forms.Label();
            this.txtFamilia = new System.Windows.Forms.TextBox();
            this.lblFamilia = new System.Windows.Forms.Label();
            this.txtInss = new System.Windows.Forms.TextBox();
            this.lblInss = new System.Windows.Forms.Label();
            this.txtLiquido = new System.Windows.Forms.TextBox();
            this.lblLiquido = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtDependentes
            // 
            this.txtDependentes.Location = new System.Drawing.Point(100, 44);
            this.txtDependentes.Name = "txtDependentes";
            this.txtDependentes.Size = new System.Drawing.Size(84, 20);
            this.txtDependentes.TabIndex = 26;
            this.txtDependentes.Text = "0";
            this.txtDependentes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDependentes
            // 
            this.lblDependentes.Location = new System.Drawing.Point(16, 44);
            this.lblDependentes.Name = "lblDependentes";
            this.lblDependentes.Size = new System.Drawing.Size(76, 20);
            this.lblDependentes.TabIndex = 25;
            this.lblDependentes.Text = "Dependentes";
            // 
            // txtFuncionario
            // 
            this.txtFuncionario.Location = new System.Drawing.Point(100, 16);
            this.txtFuncionario.Name = "txtFuncionario";
            this.txtFuncionario.Size = new System.Drawing.Size(236, 20);
            this.txtFuncionario.TabIndex = 24;
            // 
            // lblFuncionario
            // 
            this.lblFuncionario.Location = new System.Drawing.Point(16, 16);
            this.lblFuncionario.Name = "lblFuncionario";
            this.lblFuncionario.Size = new System.Drawing.Size(76, 20);
            this.lblFuncionario.TabIndex = 23;
            this.lblFuncionario.Text = "Funcionário";
            // 
            // txtBruto
            // 
            this.txtBruto.Location = new System.Drawing.Point(100, 72);
            this.txtBruto.Name = "txtBruto";
            this.txtBruto.Size = new System.Drawing.Size(84, 20);
            this.txtBruto.TabIndex = 28;
            this.txtBruto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblBruto
            // 
            this.lblBruto.Location = new System.Drawing.Point(16, 72);
            this.lblBruto.Name = "lblBruto";
            this.lblBruto.Size = new System.Drawing.Size(76, 20);
            this.lblBruto.TabIndex = 27;
            this.lblBruto.Text = "Salário bruto";
            // 
            // txtFamilia
            // 
            this.txtFamilia.Enabled = false;
            this.txtFamilia.Location = new System.Drawing.Point(100, 136);
            this.txtFamilia.Name = "txtFamilia";
            this.txtFamilia.Size = new System.Drawing.Size(84, 20);
            this.txtFamilia.TabIndex = 32;
            this.txtFamilia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFamilia
            // 
            this.lblFamilia.Location = new System.Drawing.Point(16, 136);
            this.lblFamilia.Name = "lblFamilia";
            this.lblFamilia.Size = new System.Drawing.Size(76, 20);
            this.lblFamilia.TabIndex = 31;
            this.lblFamilia.Text = "Salário família";
            // 
            // txtInss
            // 
            this.txtInss.Enabled = false;
            this.txtInss.Location = new System.Drawing.Point(100, 108);
            this.txtInss.Name = "txtInss";
            this.txtInss.Size = new System.Drawing.Size(84, 20);
            this.txtInss.TabIndex = 30;
            this.txtInss.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblInss
            // 
            this.lblInss.Location = new System.Drawing.Point(16, 108);
            this.lblInss.Name = "lblInss";
            this.lblInss.Size = new System.Drawing.Size(76, 20);
            this.lblInss.TabIndex = 29;
            this.lblInss.Text = "INSS";
            // 
            // txtLiquido
            // 
            this.txtLiquido.Enabled = false;
            this.txtLiquido.Location = new System.Drawing.Point(100, 164);
            this.txtLiquido.Name = "txtLiquido";
            this.txtLiquido.Size = new System.Drawing.Size(84, 20);
            this.txtLiquido.TabIndex = 34;
            this.txtLiquido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblLiquido
            // 
            this.lblLiquido.Location = new System.Drawing.Point(16, 164);
            this.lblLiquido.Name = "lblLiquido";
            this.lblLiquido.Size = new System.Drawing.Size(76, 20);
            this.lblLiquido.TabIndex = 33;
            this.lblLiquido.Text = "Salário líquido";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(220, 108);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 35;
            this.btnCalcular.Text = "&Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // frmExercicio06
            // 
            this.AcceptButton = this.btnCalcular;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 205);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.txtLiquido);
            this.Controls.Add(this.lblLiquido);
            this.Controls.Add(this.txtFamilia);
            this.Controls.Add(this.lblFamilia);
            this.Controls.Add(this.txtInss);
            this.Controls.Add(this.lblInss);
            this.Controls.Add(this.txtBruto);
            this.Controls.Add(this.lblBruto);
            this.Controls.Add(this.txtDependentes);
            this.Controls.Add(this.lblDependentes);
            this.Controls.Add(this.txtFuncionario);
            this.Controls.Add(this.lblFuncionario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExercicio06";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exercicio06";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmExercicio06_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtDependentes;
        private System.Windows.Forms.Label lblDependentes;
        private System.Windows.Forms.TextBox txtFuncionario;
        private System.Windows.Forms.Label lblFuncionario;
        private System.Windows.Forms.TextBox txtBruto;
        private System.Windows.Forms.Label lblBruto;
        private System.Windows.Forms.TextBox txtFamilia;
        private System.Windows.Forms.Label lblFamilia;
        private System.Windows.Forms.TextBox txtInss;
        private System.Windows.Forms.Label lblInss;
        private System.Windows.Forms.TextBox txtLiquido;
        private System.Windows.Forms.Label lblLiquido;
        private System.Windows.Forms.Button btnCalcular;
    }
}