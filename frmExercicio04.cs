﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_05
{
    public partial class frmExercicio04 : Form
    {
        public frmExercicio04()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            float bruto, liquido;
            if (txtBruto.Text != "")
            {
                bruto = float.Parse(txtBruto.Text);
                if (bruto < 500)
                {
                    liquido = bruto - (bruto * 8 / 100);
                }
                else
                {
                    liquido = bruto - (bruto * 9 / 100);
                }
                txtLiquido.Text = liquido.ToString("C2");
            }
            else{
                MessageBox.Show("Todos os campos são de preenchimento obrigatório", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtLiquido.Clear();
                txtBruto.Focus();
            }
        }

        private void frmExercicio04_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Ecerrar o programa?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No){
                e.Cancel = false;
            }
        }
    }
}
