﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_05
{
    public partial class frmExercicio03 : Form
    {
        public frmExercicio03()
        {
            InitializeComponent();
        }

        private void btnVerificar_Click(object sender, EventArgs e)
        {
            int temp;
            if (txtTemp.Text != "")
            {
                temp = int.Parse(txtTemp.Text);
                txtResultado.Clear();
                if (temp >= 37)
                {
                    txtResultado.Text = "Está com febre";
                }
            }
            else{
                MessageBox.Show("Todos os campos são de preenchimento obrigatório", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtResultado.Clear();
                txtTemp.Focus();
            }
        }

        private void frmExercicio03_Load(object sender, EventArgs e)
        {

        }

        private void frmExercicio03_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Encerrar o programa?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No){
                e.Cancel = true;
            }
        }
    }
}
