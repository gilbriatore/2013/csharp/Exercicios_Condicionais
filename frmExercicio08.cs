﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_05
{
    public partial class frmExercicio08 : Form
    {
        public frmExercicio08()
        {
            InitializeComponent();
        }

        private void btnVerificar_Click(object sender, EventArgs e)
        {
            int mes;
            if (txtMes.Text != "")
            {
                mes = int.Parse(txtMes.Text);
                switch (mes)
                {
                    case 1:
                        txtResultado.Text = "Janeiro";
                        break;
                    case 2:
                        txtResultado.Text = "Fevereiro";
                        break;
                    case 3:
                        txtResultado.Text = "Março";
                        break;
                    case 4:
                        txtResultado.Text = "Abril";
                        break;
                    case 5:
                        txtResultado.Text = "Maio";
                        break;
                    case 6:
                        txtResultado.Text = "Junho";
                        break;
                    case 7:
                        txtResultado.Text = "Julho";
                        break;
                    case 8:
                        txtResultado.Text = "Agosto";
                        break;
                    case 9:
                        txtResultado.Text = "Setembro";
                        break;
                    case 10:
                        txtResultado.Text = "Outubro";
                        break;
                    case 11:
                        txtResultado.Text = "Novembro";
                        break;
                    case 12:
                        txtResultado.Text = "Dezembro";
                        break;
                    default:
                        MessageBox.Show("Mês inválido", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtResultado.Clear();
                        txtMes.Focus();
                        break;
                }
            }
            else{
                MessageBox.Show("Todos os campos são de preenchimento obrigatório", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtResultado.Clear();
                txtMes.Focus();
            }
        }

        private void frmExercicio08_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Encerrar o programa?", "Finalizar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void txtResultado_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblResultado_Click(object sender, EventArgs e)
        {

        }

        private void txtMes_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblMes_Click(object sender, EventArgs e)
        {

        }
    }
}
