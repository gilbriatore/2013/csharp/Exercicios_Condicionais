﻿namespace Aula_05
{
    partial class frmExercicio07
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtValor3 = new System.Windows.Forms.TextBox();
            this.lblValor3 = new System.Windows.Forms.Label();
            this.txtValor2 = new System.Windows.Forms.TextBox();
            this.lblValor2 = new System.Windows.Forms.Label();
            this.txtValor1 = new System.Windows.Forms.TextBox();
            this.lblValor1 = new System.Windows.Forms.Label();
            this.txtMaior = new System.Windows.Forms.TextBox();
            this.lblMaior = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtValor3
            // 
            this.txtValor3.Location = new System.Drawing.Point(100, 68);
            this.txtValor3.Name = "txtValor3";
            this.txtValor3.Size = new System.Drawing.Size(84, 20);
            this.txtValor3.TabIndex = 34;
            this.txtValor3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblValor3
            // 
            this.lblValor3.Location = new System.Drawing.Point(16, 68);
            this.lblValor3.Name = "lblValor3";
            this.lblValor3.Size = new System.Drawing.Size(76, 20);
            this.lblValor3.TabIndex = 33;
            this.lblValor3.Text = "Valor 3";
            // 
            // txtValor2
            // 
            this.txtValor2.Location = new System.Drawing.Point(100, 40);
            this.txtValor2.Name = "txtValor2";
            this.txtValor2.Size = new System.Drawing.Size(84, 20);
            this.txtValor2.TabIndex = 32;
            this.txtValor2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblValor2
            // 
            this.lblValor2.Location = new System.Drawing.Point(16, 40);
            this.lblValor2.Name = "lblValor2";
            this.lblValor2.Size = new System.Drawing.Size(76, 20);
            this.lblValor2.TabIndex = 31;
            this.lblValor2.Text = "Valor 2";
            // 
            // txtValor1
            // 
            this.txtValor1.Location = new System.Drawing.Point(100, 12);
            this.txtValor1.Name = "txtValor1";
            this.txtValor1.Size = new System.Drawing.Size(84, 20);
            this.txtValor1.TabIndex = 30;
            this.txtValor1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblValor1
            // 
            this.lblValor1.Location = new System.Drawing.Point(16, 12);
            this.lblValor1.Name = "lblValor1";
            this.lblValor1.Size = new System.Drawing.Size(76, 20);
            this.lblValor1.TabIndex = 29;
            this.lblValor1.Text = "Valor 1";
            // 
            // txtMaior
            // 
            this.txtMaior.Enabled = false;
            this.txtMaior.Location = new System.Drawing.Point(100, 96);
            this.txtMaior.Name = "txtMaior";
            this.txtMaior.Size = new System.Drawing.Size(84, 20);
            this.txtMaior.TabIndex = 36;
            this.txtMaior.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblMaior
            // 
            this.lblMaior.Location = new System.Drawing.Point(16, 96);
            this.lblMaior.Name = "lblMaior";
            this.lblMaior.Size = new System.Drawing.Size(76, 20);
            this.lblMaior.TabIndex = 35;
            this.lblMaior.Text = "Maior valor";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(108, 128);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 37;
            this.btnCalcular.Text = "&Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // frmExercicio07
            // 
            this.AcceptButton = this.btnCalcular;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(198, 171);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.txtMaior);
            this.Controls.Add(this.lblMaior);
            this.Controls.Add(this.txtValor3);
            this.Controls.Add(this.lblValor3);
            this.Controls.Add(this.txtValor2);
            this.Controls.Add(this.lblValor2);
            this.Controls.Add(this.txtValor1);
            this.Controls.Add(this.lblValor1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExercicio07";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exercício 07";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmExercicio07_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtValor3;
        private System.Windows.Forms.Label lblValor3;
        private System.Windows.Forms.TextBox txtValor2;
        private System.Windows.Forms.Label lblValor2;
        private System.Windows.Forms.TextBox txtValor1;
        private System.Windows.Forms.Label lblValor1;
        private System.Windows.Forms.TextBox txtMaior;
        private System.Windows.Forms.Label lblMaior;
        private System.Windows.Forms.Button btnCalcular;
    }
}