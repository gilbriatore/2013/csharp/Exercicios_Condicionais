﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula_05
{
    public partial class frmExercicio07 : Form
    {
        public frmExercicio07()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int maior, valor;
            if (txtValor1.Text != "" && txtValor2.Text != "" && txtValor3.Text != "")
            {
                valor = int.Parse(txtValor1.Text);
                maior = valor;
                valor = int.Parse(txtValor2.Text);
                if (valor > maior)
                {
                    maior = valor;
                }
                valor = int.Parse(txtValor3.Text);
                if (valor > maior)
                {
                    maior = valor;
                }
                txtMaior.Text = maior.ToString();
            }
            else{
                MessageBox.Show("Todos os valores são de preenchimento obrigatório.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtMaior.Clear();
                txtValor1.Focus();
            }
        }

        private void frmExercicio07_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Encerrar o programa?", "Finalizar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}
